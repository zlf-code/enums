### 安装
`1. 环境要求:>=php8`
`2. 安装方式:composer require zlf/enums`

### 注解参数
Zlf\Enums\Enums
| 参数名称 | 是否必填 | 参数类型 | 参数说明 |
| -| - | - | - |
| label | 是 | string | 枚举得释义 |
| tag | 否 | ?string | 枚举分组,不设置时真个枚举类为同一个分组 |
| arg | 否 | array | 额外参数,和枚举数据合并为完整数据 |

### 使用示例[继承]
```php
<?php
declare(strict_types=1);

use Zlf\Enums\Enums;
use Zlf\Enums\EnumsBase;

class TestEnums extends EnumsBase
{

    const TAG = [
        'STATUS' => 'status'
    ];

    #[Enums(label: '开启', tag: self::TAG['STATUS'], arg: ['des' => '开启状态', 'color' => '#409eff'])]
    const  STATUS_OPEN = 1;


    #[Enums(label: '关闭', tag: self::TAG['STATUS'], arg: ['des' => '关闭状态', 'color' => '#ff0000'])]
    const STATUS_CLOSE = 0;
}

```


### 使用示例[Trait]
```php
<?php
declare(strict_types=1);

use Zlf\Enums\EnumsTrait;
use Zlf\Enums\Enums;

class TestEnums{
    use EnumsTrait;
  
    const TAG = [
        'STATUS' => 'status'
    ];

    #[Enums(label: '开启', tag: self::TAG['STATUS'], arg: ['des' => '开启状态', 'color' => '#409eff'])]
    const  STATUS_OPEN = 1;


    #[Enums(label: '关闭', tag: self::TAG['STATUS'], arg: ['des' => '关闭状态', 'color' => '#ff0000'])]
    const STATUS_CLOSE = 0;
}


```


### 获取全部枚举值
```php
TestEnums::Values(TestEnums::TAG['STATUS'])
Array
(
    [0] => 1
    [1] => 0
)


```
### 获取值得枚举属性
```php
TestEnums::Enum(TestEnums::STATUS_OPEN, TestEnums::TAG['STATUS'])
Array
(
    [value] => 1
    [label] => 开启
    [des] => 开启状态
    [color] => #409eff
)
```


### 获取枚举释义
```php
TestEnums::Explain(TestEnums::STATUS_OPEN, TestEnums::TAG['STATUS'])
开启
```

### 检查值是否是有效枚举值
```php
TestEnums::Exists(TestEnums::STATUS_OPEN, TestEnums::TAG['STATUS'])
true
```

### 转为字典数据
```php
TestEnums::ValuesToData(TestEnums::TAG['STATUS'])
Array
(
    [0] => Array
        (
            [value] => 1
            [label] => 开启
        )

    [1] => Array
        (
            [value] => 0
            [label] => 关闭
        )

)
```

### 获取枚举全部释义
```php
TestEnums::Labels(TestEnums::TAG['STATUS'])
Array
(
    [0] => 开启
    [1] => 关闭
)
```

### 获取完整枚举内容
```php
TestEnums::Datas(TestEnums::TAG['STATUS'])
Array
(
    [0] => Array
        (
            [value] => 1
            [label] => 开启
            [des] => 开启状态
            [color] => #409eff
        )

    [1] => Array
        (
            [value] => 0
            [label] => 关闭
            [des] => 关闭状态
            [color] => #ff0000
        )

)
```

### 获取枚举键值对数据
```php
TestEnums::Map(TestEnums::TAG['STATUS'])
Array
(
    [1] => 开启
    [0] => 关闭
)
```

### 通过label获取value
要求：版本大于等于v1.0.1
```php
TestEnums::LabelByValue('开启', TestEnums::TAG['STATUS'])
1
```