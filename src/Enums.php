<?php

declare(strict_types=1);

namespace Zlf\Enums;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS_CONSTANT)]
class Enums
{
    public string $label;

    public string $tag;

    public array $arg;

    public function __construct(string $label, ?string $tag = null, array $arg = [])
    {

    }
}