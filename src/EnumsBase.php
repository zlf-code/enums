<?php

declare(strict_types=1);

namespace Zlf\Enums;


/***
 * 枚举基类
 */
class EnumsBase
{
    /**
     * 定义状态标签
     */
    public const TAG = [];

    use EnumsTrait;
}