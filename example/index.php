<?php
declare(strict_types=1);

use Zlf\Enums\Enums;
use Zlf\Enums\EnumsBase;

require_once "../vendor/autoload.php";

class TestEnums extends EnumsBase
{

    const TAG = [
        'STATUS' => 'status'
    ];

    #[Enums(label: '开启', tag: self::TAG['STATUS'], arg: ['des' => '开启状态', 'color' => '#409eff'])]
    const  STATUS_OPEN = 1;


    #[Enums(label: '关闭', tag: self::TAG['STATUS'], arg: ['des' => '关闭状态', 'color' => '#ff0000'])]
    const STATUS_CLOSE = 0;
}

/**
 * 获取全部数据
 */

TestEnums::Values(TestEnums::TAG['STATUS']);


TestEnums::Enum(TestEnums::STATUS_OPEN, TestEnums::TAG['STATUS']);


TestEnums::Explain(TestEnums::STATUS_OPEN, TestEnums::TAG['STATUS']);


TestEnums::Exists(TestEnums::STATUS_OPEN, TestEnums::TAG['STATUS']);


TestEnums::Values(TestEnums::TAG['STATUS']);

TestEnums::ValuesToData(TestEnums::TAG['STATUS']);


TestEnums::Labels(TestEnums::TAG['STATUS']);

TestEnums::Datas(TestEnums::TAG['STATUS']);


TestEnums::Map(TestEnums::TAG['STATUS']);


TestEnums::LabelByValue('关闭', TestEnums::TAG['STATUS']);